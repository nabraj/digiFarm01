import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { NgModule, ErrorHandler } from '@angular/core';

import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';

import { InAppBrowser } from '@ionic-native/in-app-browser';
import { SplashScreen } from '@ionic-native/splash-screen';

import { IonicStorageModule } from '@ionic/storage';

import { ConferenceApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { PopoverPage } from '../pages/about-popover/about-popover';
import { AccountPage } from '../pages/account/account';
import { LoginPage } from '../pages/login/login';
import { MapPage } from '../pages/map/map';
import { SchedulePage } from '../pages/schedule/schedule';
import { ScheduleFilterPage } from '../pages/schedule-filter/schedule-filter';
import { SessionDetailPage } from '../pages/session-detail/session-detail';
import { SignupPage } from '../pages/signup/signup';
import { SpeakerDetailPage } from '../pages/speaker-detail/speaker-detail';
import { SpeakerListPage } from '../pages/speaker-list/speaker-list';
import { TabsPage } from '../pages/tabs-page/tabs-page';
import { TutorialPage } from '../pages/tutorial/tutorial';
import { SupportPage } from '../pages/support/support';
import { wait1Page } from "../pages/planting-flow/wait1/wait1";
import { wait2Page } from "../pages/planting-flow/wait2/wait2";
import { summarySoilPrep } from "../pages/planting-flow/summarySoilPrep/summarySoilPrep";
import { whatToGrowPage } from "../pages/planting-flow/whatToGrow/whatToGrow";
import { ConferenceData } from '../providers/conference-data';
import { UserData } from '../providers/user-data';

import {soilDetailBeetsPage} from "../pages/planting-flow/soilDetailBeetsPage/soilDetailBeetsPage";
import { soilDetailKalePage} from "../pages/planting-flow/soilDetailKalePage/soilDetailKalePage";
import { soilDetailLettucePage} from "../pages/planting-flow/soilDetailLettucePage/soilDetailLettucePage";
import { connectingPage } from "../pages/deviceActivation/connecting/connecting";
import { blinkingPage } from "../pages/deviceActivation/blinking/blinking";
import { setupDonePage} from "../pages/deviceActivation/setupDone/setupDone";
import { soilDonePage} from "../pages/planting-flow/soilDone/soilDone";
import { pestPage} from "../pages/health/pest/pest";
import { diseasePage} from "../pages/health/disease/disease";

@NgModule({
  declarations: [
    ConferenceApp,
    AboutPage,
    AccountPage,
    LoginPage,
    MapPage,
    PopoverPage,
    SchedulePage,
    ScheduleFilterPage,
    SessionDetailPage,
    SignupPage,
    SpeakerDetailPage,
    SpeakerListPage,
    TabsPage,
    TutorialPage,
    SupportPage,
    wait1Page,
    wait2Page,
    summarySoilPrep,
    soilDetailBeetsPage,
    soilDetailKalePage,
    soilDetailLettucePage,
    whatToGrowPage,
    connectingPage,
    blinkingPage,
    setupDonePage,
    soilDonePage,
    pestPage,
    diseasePage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(ConferenceApp, {}, {
      links: [
        { component: TabsPage, name: 'TabsPage', segment: 'tabs-page' },
        { component: SchedulePage, name: 'Schedule', segment: 'schedule' },
        { component: SessionDetailPage, name: 'SessionDetail', segment: 'sessionDetail/:sessionId' },
        { component: ScheduleFilterPage, name: 'ScheduleFilter', segment: 'scheduleFilter' },
        { component: SpeakerListPage, name: 'SpeakerList', segment: 'speakerList' },
        { component: SpeakerDetailPage, name: 'SpeakerDetail', segment: 'speakerDetail/:speakerId' },
        { component: MapPage, name: 'Map', segment: 'map' },
        { component: AboutPage, name: 'About', segment: 'about' },
        { component: TutorialPage, name: 'Tutorial', segment: 'tutorial' },
        { component: SupportPage, name: 'SupportPage', segment: 'support' },
        { component: LoginPage, name: 'LoginPage', segment: 'login' },
        { component: AccountPage, name: 'AccountPage', segment: 'account' },
        { component: SignupPage, name: 'SignupPage', segment: 'signup' }
      ]
    }),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    ConferenceApp,
    AboutPage,
    AccountPage,
    LoginPage,
    MapPage,
    PopoverPage,
    SchedulePage,
    ScheduleFilterPage,
    SessionDetailPage,
    SignupPage,
    SpeakerDetailPage,
    SpeakerListPage,
    TabsPage,
    TutorialPage,
    SupportPage,
    wait1Page,
    wait2Page,
    summarySoilPrep,
    soilDetailBeetsPage,
    soilDetailKalePage,
    soilDetailLettucePage,
    whatToGrowPage,
    connectingPage,
    blinkingPage,
    setupDonePage,
    soilDonePage,
    pestPage,
    diseasePage
  ],
  providers: [
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    ConferenceData,
    UserData,
    InAppBrowser,
    SplashScreen
  ]
})
export class AppModule { }

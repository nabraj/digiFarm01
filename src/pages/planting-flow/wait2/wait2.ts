import { Component } from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';

import { ConferenceData } from '../../../providers/conference-data';
import {summarySoilPrep} from "../summarySoilPrep/summarySoilPrep";

@Component({
  selector: 'wait2',
  templateUrl: 'wait2.html'
})
export class wait2Page {
  session: any;

  constructor(public dataProvider: ConferenceData, public navParams: NavParams, public navCtrl: NavController) {

    /*    setTimeout(function(){
      //
      this.navCtrl.push(summarySoilPrep);
    }, 3000);*/

  }

  gotoSummarySoilPrep(){
    this.navCtrl.push(summarySoilPrep);
  }

  ionViewWillEnter() {
    this.dataProvider.load().subscribe((data: any) => {
      if (
        data &&
        data.schedule &&
        data.schedule[0] &&
        data.schedule[0].groups
      ) {
        for (const group of data.schedule[0].groups) {
          if (group && group.sessions) {
            for (const session of group.sessions) {
              if (session && session.id === this.navParams.data.sessionId) {
                this.session = session;
                break;
              }
            }
          }
        }
      }
    });
  }
}
